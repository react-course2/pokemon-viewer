import { SET_POKEMON_DATA, RESET } from './pokemonConstants';

const initialState = {
  pokemon: [],
};

const pokemonReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_POKEMON_DATA:
      state = { pokemon: payload };
      return state;
    case RESET: {
      state = initialState;
      return state;
    }
    default:
      return state;
  }
};

export default pokemonReducer;
