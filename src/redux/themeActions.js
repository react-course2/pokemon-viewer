export const setTheme = (data) => (dispatch) => {
  dispatch({ type: 'SET_THEME', payload: data });
};
