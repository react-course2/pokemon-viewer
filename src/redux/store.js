import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import pokemonReducer from './pokemonReducer';
import themeReducer from './themeReducer';

const store = createStore(
  combineReducers({
    pokemonData: pokemonReducer,
    themeData: themeReducer,
  }),
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

export default store;
