const initialState = {
  backgroundColor: '#757575',
  textColor: 'red',
};

const themeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SET_THEME':
      state = { ...state, ...payload };
      return state;

    default:
      return state;
  }
};

export default themeReducer;
