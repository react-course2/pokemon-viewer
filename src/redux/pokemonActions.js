import { SET_POKEMON_DATA } from './pokemonConstants';
export const fetchAndSetPokemon = (count) => async (dispatch) => {
  try {
    const result = [];
    for (let i = 1; i <= count; i++) {
      const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${i}`);
      const fres = await res.json();
      result.push(fres);
    }
    console.log('DISPATCH', dispatch);
    dispatch({ type: SET_POKEMON_DATA, payload: result });
  } catch (error) {
    console.log('error in fetching fetchAndSetPokemon-->', error);
  }
};


