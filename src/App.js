import React, { Component } from 'react';
import SearchBar from './components/SearchBar';
import PokemonList from './components/PokemonList';
import { connect, useSelector, useDispatch } from 'react-redux';
import store from './redux/store';
import './App.css';
import { SET_POKEMON_DATA, RESET } from './redux/pokemonConstants';
import { fetchAndSetPokemon } from './redux/pokemonActions';
import { setTheme } from './redux/themeActions';

const RawPrint = () => {
  const pokemon = useSelector((state) => state.pokemon);
  const dispatch = useDispatch();
  return (
    <>
      <pre style={{ color: 'white' }}>{JSON.stringify(pokemon, null, 2)}</pre>
      <button
        onClick={() => {
          dispatch({ type: RESET });
        }}
      >
        RESET STATE
      </button>
    </>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemonCount: 0,
      isFetching: false,
      singlePokemon: null,
      showRaw: false,
    };
    console.log('PROPS', this.props);
    this.setPokemonCount = this.setPokemonCount.bind(this);
  }

  async fetchPokemon() {
    try {
      this.setState({ isFetching: true });
      this.props.fetchAndSetPokemon(this.state.pokemonCount);
      this.setState({ isFetching: false });
    } catch (error) {
      console.log('ERROR IN FETCHING POKEMON', error);
    }
  }

  setPokemonCount(val) {
    this.setState({ pokemonCount: val }, this.fetchPokemon);
  }

  render() {
    return (
      <div
        className="col"
        style={{ backgroundColor: this.props.theme.backgroundColor }}
      >
        <h2 style={{ color: this.props.theme.textColor }}>POKEMON LIST APP</h2>
        <button
          onClick={() =>
            this.props.setTheme({
              backgroundColor: '#252525',
              textColor: 'orange',
            })
          }
        >
          CHANGE THEME
        </button>
        {/* SEARCH BAR */}
        <SearchBar
          setPokemonCount={this.setPokemonCount}
          isFetching={this.state.isFetching}
        />
        {/* RAW */}
        <input
          type="checkbox"
          checked={this.state.showRaw}
          onChange={(ev) => this.setState({ showRaw: !this.state.showRaw })}
        />
        {this.state.showRaw ? <RawPrint /> : null}

        {/* POKEMON LIST */}
        {this.state.isFetching ? (
          <h6>Loading.....</h6>
        ) : (
          <PokemonList pokemon={this.props.pokemon} />
        )}
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    pokemon: store.pokemonData.pokemon,
    theme: store.themeData,
  };
};

export default connect(mapStateToProps, { fetchAndSetPokemon, setTheme })(App);
