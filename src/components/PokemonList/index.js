import React from 'react';

export default function PokemonList({ pokemon }) {
  return (
    <div className="row" style={{flexWrap:"wrap"}}>
      {pokemon.map((p) => (
        <div className="card" key={p.name}>
          <img
            src={p.sprites.back_default}
            alt="Avatar"
            style={{ width: 100, height: 100 }}
          />
          <div className="container">
            <h4>
              <b>{p.name}</b>
            </h4>
            <p>{p.types.map((a) => a.type.name).join(', ')}</p>
          </div>
        </div>
      ))}
    </div>
  );
}
