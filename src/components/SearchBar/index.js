import React from 'react';
import PropTypes from 'prop-types';
let count = 0;
export default function SearchBar(props) {
  return (
    <div className="row" style={{ margin: 32 }}>
      <input
        disabled={props.isFetching}
        placeholder={3}
        type="number"
        onChange={(event) => {
          count = event.target.value;
        }}
      />
      <button
        disabled={props.isFetching}
        onClick={() => {
          props.setPokemonCount(count);
        }}
      >
        GET POKEMON
      </button>
    </div>
  );
}

SearchBar.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  setPokemonCount: PropTypes.func.isRequired,
};
